# Generated by Django 3.0.2 on 2020-01-22 00:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('compras', '0007_remove_factura_número'),
    ]

    operations = [
        migrations.AddField(
            model_name='factura',
            name='zaraza',
            field=models.CharField(default='1', max_length=10),
        ),
    ]
