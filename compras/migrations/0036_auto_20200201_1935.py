# Generated by Django 3.0.2 on 2020-02-01 22:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('compras', '0035_auto_20200201_1926'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='factura',
            name='detalle',
        ),
        migrations.AddField(
            model_name='factura',
            name='detalle',
            field=models.ManyToManyField(blank=True, null=True, to='compras.Detalle'),
        ),
    ]
