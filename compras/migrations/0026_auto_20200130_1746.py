# Generated by Django 3.0.2 on 2020-01-30 20:46

from django.db import migrations, models
import model_utils.fields


class Migration(migrations.Migration):

    dependencies = [
        ('compras', '0025_auto_20200130_1615'),
    ]

    operations = [
        migrations.RenameField(
            model_name='detalle',
            old_name='producto',
            new_name='nombre',
        ),
        migrations.AlterField(
            model_name='factura',
            name='estado',
            field=models.CharField(choices=[('PAGA', 'PAGA'), ('IMPAGA', 'IMPAGA')], default='IMPAGA', max_length=200),
        ),
        migrations.AlterField(
            model_name='factura',
            name='fecha_de_pago',
            field=model_utils.fields.MonitorField(blank=True, default=None, monitor='estado', null=True, verbose_name='Fecha de pago', when={'PAGA'}),
        ),
    ]
