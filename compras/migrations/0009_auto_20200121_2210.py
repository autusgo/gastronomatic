# Generated by Django 3.0.2 on 2020-01-22 01:10

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('compras', '0008_factura_zaraza'),
    ]

    operations = [
        migrations.RenameField(
            model_name='factura',
            old_name='zaraza',
            new_name='numero',
        ),
    ]
