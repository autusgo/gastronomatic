# Generated by Django 3.0.2 on 2020-01-20 23:35

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('compras', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='producto',
            old_name='presentacion',
            new_name='tipo',
        ),
    ]
