# Generated by Django 3.0.2 on 2020-01-24 01:05

from django.db import migrations, models
import model_utils.fields


class Migration(migrations.Migration):

    dependencies = [
        ('compras', '0011_auto_20200123_2144'),
    ]

    operations = [
        migrations.AddField(
            model_name='factura',
            name='total',
            field=models.DecimalField(blank=True, decimal_places=2, default=0, max_digits=10, verbose_name='total'),
        ),
        migrations.AlterField(
            model_name='factura',
            name='estado',
            field=models.CharField(choices=[('nopago', 'NO PAGO'), ('pago', 'PAGO')], default='nopago', max_length=64, verbose_name='estado'),
        ),
        migrations.AlterField(
            model_name='factura',
            name='fecha_de_pago',
            field=model_utils.fields.MonitorField(blank=True, default=None, monitor='estado', null=True, verbose_name='Fecha de pago', when={'pago'}),
        ),
    ]
